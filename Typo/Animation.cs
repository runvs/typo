﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Typo
{
    class Animation : GameObject
    {

        Texture2D animationTexture;

        private int animationFrameWidth;
        private int animationFrameHeight;
        private float animationFrameTime;
        private int index;
        private bool playing;

        public Vector2 Velocity { get; set; }
        public float VelocityDamping { get; set; }  // 0 means no damping, 1 means full damping

        public float ScaleChange { get; set; }

        public float RoationVelocity { get; set; }

        public float Alpha { get { return alpha; } set { alpha = AlphaStart = value; } }
        private float alpha;
        private float AlphaStart { get; set; }
        public bool AlphaDecrease { get; set; }


        private float timer;
        private int numberOfFrames;


        private Vector2 animationPosition;
        private float animationScale;
        private float animationRotation;


        public bool Over { get; private set; }


        public void Initialize()
        {
            Velocity = new Vector2();
            RoationVelocity = 0.0f;
            VelocityDamping = 0.0f;
            Alpha = 1.0f;
            AlphaDecrease = false;

            ScaleChange = 0.0f;
        }



        public void PlayAnimationOnce(Vector2 position, float scale, float rotation)
        {
            timer = 0.0f;
            index = 0;

            playing = true;
            animationPosition = position;
            animationScale = scale;
            animationRotation = rotation;
        }


        public void LoadAnimation(ContentManager content, string resourcePath, int frameWidth, float frameTime)
        {
            animationTexture = content.Load<Texture2D>(resourcePath);
            animationFrameWidth = frameWidth;
            animationFrameTime = frameTime;

            numberOfFrames = animationTexture.Width / frameWidth - 1;
            animationFrameHeight = animationTexture.Height;
            Over = false;

        }


        public void LoadContent(ContentManager content)
        {
        }

        public void Update(float deltaT)
        {
            if (playing)
            {

                if (AlphaDecrease)
                {
                    float remainingFrameTime = animationFrameTime - timer;
                    alpha = AlphaStart * (remainingFrameTime / animationFrameTime);
                }

                animationScale += ScaleChange * deltaT;

                animationPosition += Velocity * deltaT;
                Velocity *= (1.0f - VelocityDamping);

                animationRotation += RoationVelocity * deltaT;
                timer += deltaT;
                if (timer >= animationFrameTime)
                {
                    timer = 0;
                    index++;
                    if (index > numberOfFrames)
                    {
                        playing = false;
                        Over = true;
                    }
                }
            }
        }

        public void Draw(SpriteBatch spb)
        {
            if (playing)
            {
                Vector2 screenposition = animationPosition - Camera.CameraTotalPosition;
                Rectangle destination = new Rectangle((int)(screenposition.X) - animationFrameWidth / 2, (int)(screenposition.Y) - animationFrameHeight / 2, animationFrameWidth, animationFrameHeight);
                Rectangle source = new Rectangle(index * animationFrameWidth, 0, 16, 16);
                spb.Draw(animationTexture, animationPosition, source, new Color(Color.White, Alpha), animationRotation, new Vector2(8, 8), animationScale, SpriteEffects.None, 1.0f);
            }
        }

        public void GetInput(Microsoft.Xna.Framework.Input.KeyboardState KBS)
        {
            throw new NotImplementedException();
        }


    }
}
