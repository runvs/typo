﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Typo
{
    public static class GFXManager
    {

        static List<Animation> animationList;
        static ContentManager content;


        public static void Initialize()
        {
            animationList = new List<Animation>();
            NameTypedEventManager.OnSend += playShootAnimOnEnemy;
        }


        private static void playShootAnimOnEnemy(Enemy enemy)
        {
            SpawnCloudRing(enemy.Position, 10f, 0.75f);
            StartHitAnim(enemy.Position, 2.5f, (float)(Math.PI) - enemy.Angle);
        }

        public static void LoadContent(ContentManager c)
        {
            content = c;
            //Color[] data = new Color[VignetteTexture.Width * VignetteTexture.Height];
            //VignetteTexture.GetData(data);
            //data[]

        }

        public static void Draw(SpriteBatch spb)
        {
            foreach (var a in animationList)
            {
                a.Draw(spb);
            }
        }


        public static void SpawnCloudRing(Vector2 center, float ringRadius, float scale)
        {
            for (int i = 0; i != 20; i++)
            {
                Vector2 dir = RandomGenerator.GetRandomVector2OnCircle(ringRadius);
                Vector2 puffpos = center + dir;
                dir.Normalize();
                SpawnCloud(puffpos, dir * GameProperties.CloudMovementSpeed * scale, scale, RandomGenerator.NextFloat(-1.5f, 1.5f));
            }
        }

        public static void SpawnCloud(Vector2 position, Vector2 velocity, float scale, float rotationSpeed)
        {
            Animation cloudAnim = new Animation();
            cloudAnim.Initialize();
            cloudAnim.LoadAnimation(content, "GFX/cloud1", 16, 1.0f);
            cloudAnim.Velocity = velocity;
            cloudAnim.VelocityDamping = 0.05f;
            cloudAnim.Alpha = RandomGenerator.NextFloat(0.15f, 0.3f);
            cloudAnim.ScaleChange = 1.4f;
            cloudAnim.AlphaDecrease = true;
            cloudAnim.RoationVelocity = rotationSpeed;
            cloudAnim.PlayAnimationOnce(position, scale, RandomGenerator.NextFloat(0, 3.141f)); ;
            animationList.Add(cloudAnim);
        }

        public static void StartHitAnim(Vector2 position, float scale, float rotation)
        {
            Animation hitanim = new Animation();
            hitanim.Initialize();
            hitanim.LoadAnimation(content, "GFX/shot", 16, 0.08f);
            scale += RandomGenerator.NextFloat(-0.2f, 0.2f);
            hitanim.PlayAnimationOnce(position, scale, rotation);
            animationList.Add(hitanim);
        }

        public static void Update(float deltaT)
        {
            List<Animation> newAnimationList = new List<Animation>();
            foreach (var a in animationList)
            {
                a.Update(deltaT);
                if (!a.Over)
                {
                    newAnimationList.Add(a);
                }
            }

            animationList = newAnimationList;
        }


    }
}
