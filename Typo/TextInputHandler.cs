﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Typo
{
    class TextInputHandler : GameObject
    {

        private string IsTyping;

        public bool lastNameFetched = false;
        private string lastName;

        private SpriteFont font;

        private Texture2D bubbleTexture;

        private string ExtraMessage;

        private float ExtraMessageDisplayTime;
        private float totalTime;

        public void Initialize()
        {
            IsTyping = "";
            ExtraMessage = "";
            ExtraMessageDisplayTime = 0.0f;
            totalTime = 0.0f;
        }

        public string GetLastName()
        {
            if (!lastNameFetched)
            {
                lastNameFetched = true;
                return lastName;
            }
            else
            {
                return "";
            }
        }


        public void LoadContent(ContentManager Content)
        {
            font = Content.Load<SpriteFont>("Font");
            bubbleTexture = Content.Load<Texture2D>("GFX/bubble");
        }

        public void GetInput(KeyboardState KBS)
        {
            foreach (var kvp in KeyboardStateToChar.Map)
            {
                if (KeyEvents.Pressed(kvp.Key))
                {

                    char putchar = KeyboardStateToChar.Convert(kvp.Key);
                    AppendChar(putchar);
                }
            }
            if (KBS.IsKeyDown(Keys.Enter))
            {
                lastName = IsTyping;
                lastNameFetched = false;
                ClearString();
            }
        }

        public void ClearString()
        {
            IsTyping = "";
        }
        public void SetString(string str)
        {
            IsTyping = str;
        }


        public void Update(float deltaT)
        {
            Console.WriteLine(ExtraMessageDisplayTime);
            if (ExtraMessageDisplayTime > 0)
            {
                ExtraMessageDisplayTime -= deltaT;
            }
            totalTime += deltaT;
        }

        public void ShowExtraMessage(string message, float time = 5.0f)
        {
            ExtraMessage = message;
            ExtraMessageDisplayTime = time;
        }
        public void ClearExtraMessage()
        {
            ExtraMessageDisplayTime = -1.0f;
        }


        public void AppendChar(char c)
        {
            if (IsTyping.Length < 11)
            {
                IsTyping += c;
            }
        }

        public void Draw(SpriteBatch spb)
        {
            Vector2 screenposition = new Vector2(420, 335) +
                    new Vector2(1f * (float)(Math.Sin(totalTime + 4.2f)),
                        3.0f * (float)(Math.Sin(totalTime * 0.75f)));
            Rectangle dest = new Rectangle((int)(screenposition.X) - 24, (int)(screenposition.Y) - 24, 304, 64);
            Rectangle source = new Rectangle(0, 0, 152, 32);

            float alphaval = 0.1f;
            alphaval += 0.19f * (float)(IsTyping.Length);
            alphaval = (alphaval < 1.0f) ? alphaval : 1.0f;
            spb.Draw(bubbleTexture, dest, source, new Color(Color.White, alphaval));
            int count = IsTyping.Split('\n').Length - 1;
            screenposition.Y -= 15 * count;
            spb.DrawString(font, IsTyping, screenposition, Color.Black);

            if (ExtraMessageDisplayTime >= 0)
            {
                Vector2 messageposition = new Vector2(350, 250) +
                    new Vector2(
                        3.0f * (float)(Math.Sin(ExtraMessageDisplayTime * 2 + 4.2f)),
                        12.0f * (float)(Math.Sin(ExtraMessageDisplayTime * 1.5f)));
                Color c = new Color(119, 58, 40);
                spb.DrawString(font, ExtraMessage, messageposition, c);
            }

        }


    }
}
