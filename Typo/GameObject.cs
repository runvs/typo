﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Typo
{
    interface GameObject
    {
        void Initialize();
        void LoadContent(ContentManager content);
        void Update(float deltaT);
        void Draw(SpriteBatch spb);
        void GetInput(KeyboardState KBS);
    }
}
