﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Typo
{
    public class Enemy : GameObject
    {


        public void SetRandomStartingPosition()
        {
            position = new Vector2(400, 400) + RandomGenerator.GetRandomVector2OnCircle(350.0f);
        }

        public Vector2 Position { get { return position; } private set { position = value; } }

        private SpriteFont font;

        Vector2 position;
        Vector2 MoveInOffset;
        Vector2 target;

        public bool Dead { get; private set; }
        public bool Dying { get; private set; }
        private float dyingPushbackTimer;
        private float dyingFadeTimer;
        private float dyingFadeTimerMax;

        public float MovingInTimer { get; private set; }
        private float MovingInTimerMax;


        // Movement Timers (enemies will move in Waves)
        private float remainingTimeTilJump;
        private float remainingJumpTime;

        public string Name { get; private set; }

        public float Angle { get { return angle; } }

        float angle;
        SpriteDirection dir;
        private Texture2D enemyTextureRight;
        private Texture2D enemyTextureUp;
        private Texture2D enemyTextureLeft;
        private Texture2D enemyTextureDown;

        public bool blinking { get; private set; }
        float blinkTimer;


        public void Kill()
        {
            Dying = true;
            dyingPushbackTimer = GameProperties.EnemyDyingPushbackTimer;
            dyingFadeTimerMax = GameProperties.EnemyDyingFadeTimerMax;
            dyingFadeTimer = dyingFadeTimerMax;
        }


        public void Initialize()
        {
            position = new Vector2();
            target = GameProperties.ScreenSize * 0.5f;
            angle = 0;
            Dead = false;
            Dying = false;

            Name = NameGenerator.GetName();
            remainingTimeTilJump = GameProperties.EnemyJumpInterval * (1.0f + RandomGenerator.NextFloat(-0.05f, 0.05f));

            MovingInTimer = MovingInTimerMax = GameProperties.EnemyMoveInTimer;

            MoveInOffset = new Vector2(0, GameProperties.ScreenSize.Y);

        }

        public void LoadContent(ContentManager content)
        {
            enemyTextureDown = content.Load<Texture2D>("GFX/enemy");
            enemyTextureUp = content.Load<Texture2D>("GFX/enemy");
            enemyTextureLeft = content.Load<Texture2D>("GFX/enemy");
            enemyTextureRight = content.Load<Texture2D>("GFX/enemy");
            font = content.Load<SpriteFont>("Font");
        }

        public void Update(float deltaT)
        {
            angle = GameProperties.GetAngleBewtweenPoints(position, target);
            SetDirection();

            if (blinking)
            {
                blinkTimer += deltaT;
            }

            if (MovingInTimer > 0)
            {
                MovingInTimer -= deltaT;
                MoveInOffset.Y = -(float)(PennerDoubleAnimation.GetValue(PennerDoubleAnimation.EquationType.CubicEaseOut, MovingInTimer, 0, GameProperties.ScreenSize.Y, MovingInTimerMax));

                return;
            }
            else if (MovingInTimer == -12345.0f)
            {

            }
            else
            {
                MoveInOffset.Y = 0;
                GFXManager.SpawnCloudRing(position, 10.0f, 1.5f);
                Camera.ScreenShake(5, 0.25f, 0.025f);
                MovingInTimer = -12345.0f;
            }

            if (!Dead && !Dying)
            {
                if (remainingTimeTilJump > 0)
                {
                    remainingTimeTilJump -= deltaT;
                    remainingJumpTime = GameProperties.EnemyJumpTime * (1.0f + RandomGenerator.NextFloat(-0.05f, 0.05f));
                }
                else
                {
                    if (remainingJumpTime > 0)
                    {
                        remainingJumpTime -= deltaT;
                        Vector2 direction = target - position;
                        direction.Normalize();
                        position += direction * deltaT * GameProperties.EnemyMoveSpeed;

                        //angle = GameProperties.GetAngleBewtweenPoints(position, target);
                    }
                    else
                    {
                        remainingTimeTilJump = GameProperties.EnemyJumpInterval * (1.0f + RandomGenerator.NextFloat(-0.05f, 0.05f));
                    }
                }


            }

            if (Dying)
            {
                //Console.WriteLine(dyingPushbackTimer + " " + dyingFadeTimer);
                if (dyingPushbackTimer >= 0)
                {
                    dyingPushbackTimer -= deltaT;
                    Vector2 direction = -(target - position);
                    direction.Normalize();
                    position += direction * deltaT * dyingPushbackTimer * dyingPushbackTimer * GameProperties.EnemyPushbackSpeed;
                }
                else
                {
                    if (dyingFadeTimer > 0)
                    {
                        dyingFadeTimer -= deltaT;
                    }

                    if (dyingFadeTimer < 0)
                    {
                        dyingFadeTimer = 0;
                        Dead = true;
                    }
                }
            }

        }

        public void StartMovementNow()
        {
            remainingTimeTilJump = 0;
            remainingJumpTime = GameProperties.EnemyJumpTime * (1.0f + RandomGenerator.NextFloat(-0.05f, 0.05f));
        }

        public void DrawNameText(SpriteBatch spb)
        {
            if (MovingInTimer < 0 && !Dying)
            {
                spb.DrawString(font, Name, position + new Vector2(0, -46), new Color(201, 224, 146));
            }
        }

        private void SetDirection()
        {
            if (angle >= 1.0 / 4.0 * Math.PI && angle < 3.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_UP;
            }
            else if (angle >= 3.0 / 4.0 * Math.PI && angle < 5.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_LEFT;
            }
            else if (angle >= 5.0 / 4.0 * Math.PI && angle < 7.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_DOWN;
            }
            else
            {
                dir = SpriteDirection.SD_RIGHT;
            }
        }

        public void Draw(SpriteBatch spb)
        {
            Texture2D currentTexture;
            if (dir == SpriteDirection.SD_DOWN)
            {
                currentTexture = enemyTextureDown;
            }
            else if (dir == SpriteDirection.SD_LEFT)
            {
                currentTexture = enemyTextureLeft;
            }
            else if (dir == SpriteDirection.SD_RIGHT)
            {
                currentTexture = enemyTextureRight;
            }
            else
            {
                currentTexture = enemyTextureUp;
            }
            Vector2 screenposition = position + MoveInOffset - Camera.CameraTotalPosition;
            Rectangle dest = new Rectangle((int)(screenposition.X) - 16, (int)(screenposition.Y) - 16, 32, 32);
            Rectangle source = new Rectangle(0, 0, 8, 8);

            if (Dying)
            {
                if (dyingPushbackTimer >= 0)
                {

                }
                else
                {

                }
                int newAlpha = (int)((float)(255 * dyingFadeTimer / dyingFadeTimerMax));
                Color col = new Color(Color.White, newAlpha);   // fade
                spb.Draw(currentTexture, dest, source, col);

                return;
            }
            if (!Dead)
            {

                float factor = 0.5f + (float)Math.Cos(Math.PI + blinkTimer * GameProperties.EnemyBlinkFrequency) * 0.5f;
                Color c = new Color();
                if (blinking)
                {
                    c.R = 255;
                    c.G = (byte)(120 + factor * 125.0f);
                    c.B = (byte)(120 + factor * 125.0f);
                    c.A = 255;
                }
                else
                {
                    c = Color.White;
                }
                spb.Draw(currentTexture, dest, source, c);
            }

        }

        public void GetInput(Microsoft.Xna.Framework.Input.KeyboardState KBS)
        {
            throw new NotImplementedException();
        }

        internal void StartBlinking()
        {
            blinking = true;
        }


    }
}
