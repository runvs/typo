﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Typo
{
    public static class GameProperties
    {
        public static Vector2 ScreenSize { get { return new Vector2(800, 800); } }

        public static float EnemyMoveSpeed { get { return 50.0f; } }
        public static float EnemyPushbackSpeed { get { return 600.0f; } }

        public static float EnemyJumpInterval { get { return 1.5f; } }
        public static float EnemyJumpTime { get { return 0.9f; } }

        public static float EnemyDyingPushbackTimer { get { return 0.5f; } }
        public static float EnemyDyingFadeTimerMax { get { return 0.3f; } }

        public static float EnemyMoveInTimer { get { return 1.5f + RandomGenerator.NextFloat(-0.35f, 0.35f); } }

        public static float EnemyBlinkFrequency { get { return 4.5f; } }

        public static float EnemySpawnerExponent { get { return 2.5f; } }
        public static float EnemySpawnerMaxTime { get { return 4.5f; } }


        public static float GetAngleBewtweenPoints(Vector2 position, Vector2 target)
        {
            Vector2 vector = target - position;
            vector.Y = -vector.Y;

            float angle = (float)(Math.Atan(vector.Y / vector.X) + ((Math.Sign(vector.X) < 0) ? Math.PI : 0));  // calc angle by atan, note confinement of atan to [-90,90]
            // just in case, bend back angle to [0,360]
            while (angle > 2.0 * Math.PI)
            {
                angle -= 2.0f * (float)(Math.PI);
            }
            return angle;
        }


        public static float CloudMovementSpeed { get { return 75.0f; } }



        public static float IntroFadeInTimeMax { get { return 2.5f; } }




    }
}
