﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace Typo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<Enemy> enemyList;

        Texture2D background;

        Player player;
        TextInputHandler tih;

        float enemySpawningTimer = 1.5f;

        bool introPassed;
        float introFadeInTime;
        Vector2 BackgroundPosition;
        bool introHasPressedEnter;   // Player needs to press enter first;

        bool IsInGameOverState = false;
        private int NumberOfKills;
        private int NumberOfKillsSinceLastProgression;
        private float progressionLevel;
        float MaxEnemies;

        bool TriggeredThisFrame;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.Window.Title = "Typo";
            this.Window.AllowUserResizing = false;
            graphics.PreferredBackBufferWidth = (int)(GameProperties.ScreenSize.X);
            graphics.PreferredBackBufferHeight = (int)(GameProperties.ScreenSize.Y);
            graphics.IsFullScreen = false;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();


        }

        private void InitializeGameVariablesForNewGame()
        {
            introPassed = false;
            introHasPressedEnter = false;
            introFadeInTime = GameProperties.IntroFadeInTimeMax;
            BackgroundPosition = new Vector2(0, 900);

            IsInGameOverState = false;
            NumberOfKills = 0;

            MaxEnemies = 3;
            progressionLevel = 1.0f;
            enemyList = new List<Enemy>();
            NameGenerator.Initialize();
            KeyEvents.Initialize();
            tih.ShowExtraMessage("Press Enter!", 11.0f);
        }

        float GetSpawnTime()
        {
            float currentEnemies = enemyList.Count;
            float exponent = GameProperties.EnemySpawnerExponent;
            float ret = (float)(Math.Pow(currentEnemies / MaxEnemies, exponent)) * GameProperties.EnemySpawnerMaxTime;
            return ret;
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            player = new Player();
            player.Initialize();



            tih = new TextInputHandler();
            tih.Initialize();

            NameGenerator.Initialize();
            GFXManager.Initialize();

            NameTypedEventManager.OnSend += ShakeScreen;
            InitializeGameVariablesForNewGame();


            base.Initialize();


        }

        private void MakeEnemiesMove()
        {
            foreach (var e in enemyList)
            {
                e.StartMovementNow();
            }
        }

        private void ShakeScreen(Enemy enemy)
        {
            //Camera.ScreenShake(4, 0.25f, 0.025f);
            Camera.OffsetScreen(0.03f * (enemy.Position - GameProperties.ScreenSize / 2.0f));
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            player.LoadContent(this.Content);

            tih.LoadContent(this.Content);
            GFXManager.LoadContent(this.Content);

            background = Content.Load<Texture2D>("GFX/background");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            TriggeredThisFrame = false;
            #region Input

            KeyEvents.UpdateInput();

            KeyboardState KBS = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (!IsInGameOverState)
            {
                if (KeyEvents.Pressed(Keys.Enter))
                {
                    TriggeredThisFrame = true;
                }
                KeyboardStateToChar.SetMetaKeys(KBS);
                player.GetInput(KBS);
                tih.GetInput(KBS);
            }
            else
            {
                if (KBS.IsKeyDown(Keys.Enter))
                {
                    InitializeGameVariablesForNewGame();
                }
            }
            if (!introPassed)
            {
                if (KBS.IsKeyDown(Keys.Enter))
                {
                    introHasPressedEnter = true;
                    tih.ClearExtraMessage();
                    tih.ClearString();
                }
            }


            #endregion Input

            #region Update
            float elapsedTime = (float)(gameTime.ElapsedGameTime.Milliseconds) / 1000.0f;
            Camera.Update(elapsedTime);
            GFXManager.Update(elapsedTime);
            tih.Update(elapsedTime);
            DoGameProgression();

            if (!introPassed)   // in the intro
            {

                if (introHasPressedEnter)
                {
                    introFadeInTime -= elapsedTime;
                    BackgroundPosition.Y = 900 * (1.0f - (float)PennerDoubleAnimation.GetValue(PennerDoubleAnimation.EquationType.CubicEaseOut, GameProperties.IntroFadeInTimeMax - introFadeInTime, 0, 1, GameProperties.IntroFadeInTimeMax));
                    if (introFadeInTime <= 0)
                    {
                        tih.ShowExtraMessage("Let's get this started!");
                        introPassed = true;
                        BackgroundPosition.Y = 0;
                    }
                }
                else
                {
                    tih.SetString("Every Time you make a Typo\nthe errorists win.");

                }

            }
            else if (!IsInGameOverState)
            {
                enemySpawningTimer += elapsedTime;
                if (enemySpawningTimer >= GetSpawnTime())
                {
                    int spawnNumber = 1 + +((RandomGenerator.NextFloat(0, 10.0f) < progressionLevel) ? 1 : 0) + ((RandomGenerator.NextFloat(0, 10.0f) < progressionLevel) ? 1 : 0) + ((RandomGenerator.NextFloat(0, 20.0f) < progressionLevel) ? 1 : 0) + ((RandomGenerator.NextFloat(0, 30.0f) < progressionLevel) ? 1 : 0);
                    SpawnEnemyBundle(spawnNumber);
                    enemySpawningTimer = 0;
                }

                player.Update(elapsedTime);

                NameGenerator.Update(elapsedTime);
                string name = "";
                if (!tih.lastNameFetched)
                {
                    name = tih.GetLastName();
                }

                List<Enemy> newEnemyList = new List<Enemy>();

                bool foundOneEnemy = false;

                foreach (var e in enemyList)
                {
                    if (e.Name.Equals(name))
                    {
                        NameTypedEventManager.Call(e);
                        e.Kill();
                    }
                    if (TriggeredThisFrame)
                    {
                        if (e.Name.Equals(name))
                        {
                            foundOneEnemy = true;
                        }
                    }
                    else
                    {
                        foundOneEnemy = true;
                    }
                    e.Update(elapsedTime);

                    if (!e.Dead)
                    {
                        newEnemyList.Add(e);
                    }
                    else
                    {
                        NumberOfKills++;
                        NumberOfKillsSinceLastProgression++;
                    }
                    Vector2 DistanceToCenter = e.Position - player.position;
                    float lenght = DistanceToCenter.LengthSquared();
                    if (lenght <= 152 * 152)
                    {
                        e.StartBlinking();
                    }
                    if (lenght <= 13 * 13)
                    {
                        IsInGameOverState = true;
                        tih.SetString("Game Over\nYou scored " + NumberOfKills);
                    }
                }
                enemyList = newEnemyList;
                if (foundOneEnemy == false)
                {
                    MakeEnemiesMove();
                }
            }
            #endregion Update

            base.Update(gameTime);
        }



        private void DoGameProgression()
        {
            //Console.WriteLine(MaxEnemies);
            if (NumberOfKillsSinceLastProgression >= 7 * progressionLevel)
            {
                progressionLevel += 0.15f;
                NumberOfKillsSinceLastProgression = 0;
                MaxEnemies++;
                tih.ShowExtraMessage("More Enemies!");
                SpawnEnemyBundle(3);
            }

            if (MaxEnemies >= 9)
            {
                tih.ShowExtraMessage("It's getting harder! New Names icoming.");
                MaxEnemies = 3 * (float)(Math.Sqrt(progressionLevel));
                progressionLevel = 1.0f;
                NameGenerator.IncreaseDifficulty();
                if (NameGenerator.Difficulty == 2)
                {
                    tih.ShowExtraMessage("Watch out for capitals!");
                }
            }
        }





        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            SpriteBatch spb = new SpriteBatch(GraphicsDevice);


            spb.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone);
            if (!IsInGameOverState)
            {
                DrawBackground(spb);
                player.Draw(spb);
                tih.Draw(spb);


                foreach (var e in enemyList)
                {
                    e.Draw(spb);
                }

                GFXManager.Draw(spb);
                spb.End();
                spb.Begin();
                foreach (var e in enemyList)
                {
                    e.DrawNameText(spb);
                }

            }
            else
            {
                tih.Draw(spb);
            }
            spb.End();

            base.Draw(gameTime);
        }

        private void DrawBackground(SpriteBatch spb)
        {
            Vector2 screenposition = BackgroundPosition - Camera.CameraTotalPosition;
            spb.Draw(background, new Rectangle((int)(screenposition.X), (int)(screenposition.Y), (int)(GameProperties.ScreenSize.X), (int)(GameProperties.ScreenSize.Y)), Color.White);
        }

        private void SpawnEnemyBundle(int number)
        {
            for (int i = 0; i != number; i++)
            {
                SpawnEnemy();
            }
        }

        private void SpawnEnemy()
        {
            Enemy enemy = new Enemy();
            enemy.Initialize();
            enemy.LoadContent(Content);
            enemy.SetRandomStartingPosition();
            enemyList.Add(enemy);
        }

    }
}
