﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Typo
{
    public static class KeyboardStateToChar
    {
        public static Dictionary<Keys, char> Map { get { if (!initialized) { Initialize(); } return map; } }

        private static Dictionary<Keys, char> map;
        private static bool initialized = false;

        private static void Initialize()
        {
            map = new Dictionary<Keys, char>();

            map.Add(Keys.A, 'a');
            map.Add(Keys.B, 'b');
            map.Add(Keys.C, 'c');
            map.Add(Keys.D, 'd');
            map.Add(Keys.E, 'e');
            map.Add(Keys.F, 'f');
            map.Add(Keys.G, 'g');
            map.Add(Keys.H, 'h');
            map.Add(Keys.I, 'i');
            map.Add(Keys.J, 'j');
            map.Add(Keys.K, 'k');
            map.Add(Keys.L, 'l');
            map.Add(Keys.M, 'm');
            map.Add(Keys.N, 'n');
            map.Add(Keys.O, 'o');
            map.Add(Keys.P, 'p');
            map.Add(Keys.Q, 'q');
            map.Add(Keys.R, 'r');
            map.Add(Keys.S, 's');
            map.Add(Keys.T, 't');
            map.Add(Keys.U, 'u');
            map.Add(Keys.V, 'v');
            map.Add(Keys.W, 'w');
            map.Add(Keys.X, 'x');
            map.Add(Keys.Y, 'y');
            map.Add(Keys.Z, 'z');

            initialized = true;
        }

        private static bool shiftPressed;

        public static void SetMetaKeys(KeyboardState KBS)
        {
            if (KBS.IsKeyDown(Keys.LeftShift) || KBS.IsKeyDown(Keys.RightShift))
            {
                shiftPressed = true;
            }
            else
            {
                shiftPressed = false;
            }
        }

        public static void ResetMetaKeys()
        {
            shiftPressed = false;
        }


        public static char Convert(Keys k)
        {
            char retVal = '1';

            if (!initialized)
            {
                Initialize();
            }


            if (map.ContainsKey(k))
            {
                retVal = map[k];
            }

            if (shiftPressed)
            {
                retVal = (char)((int)(retVal) - 32);    // hacky
            }

            return retVal;
        }

    }
}
