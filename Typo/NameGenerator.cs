﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Typo
{
    public static class NameGenerator
    {
        private static float totalTime = 0.0f;

        private static Dictionary<int, List<string>> names;

        private static int currentDifficulty;


        public static string GetName()
        {
            return GetRandomName(currentDifficulty);
        }

        private static string GetRandomName(int difficulty)
        {
            return names[difficulty][RandomGenerator.Random.Next(0, names[difficulty].Count)];
        }

        internal static int Difficulty { get { return currentDifficulty; } }

        internal static void IncreaseDifficulty()
        {
            currentDifficulty++;
            if (!names.ContainsKey(currentDifficulty))
            {
                currentDifficulty--;
            }
        }

        internal static void Initialize()
        {
            totalTime = 0.0f;
            currentDifficulty = 0;

            CreateNames();
        }

        private static void CreateNames()
        {
            names = new Dictionary<int, List<string>>();
            List<string> names0 = new List<string>();
            names0.Add("ada");
            names0.Add("abe");
            names0.Add("ace");
            names0.Add("al");
            names0.Add("ash");




            names0.Add("bay");
            names0.Add("bea");
            names0.Add("ben");
            names0.Add("bev");
            names0.Add("bob");

            names0.Add("cal");
            names0.Add("coe");
            names0.Add("coy");
            names0.Add("cy");

            names0.Add("dan");
            names0.Add("dax");
            names0.Add("deb");
            names0.Add("dee");
            names0.Add("dex");
            names0.Add("di");

            names0.Add("ed");
            names0.Add("eli");
            names0.Add("eve");

            names0.Add("fen");
            names0.Add("flo");
            names0.Add("fox");

            names0.Add("gil");
            names0.Add("gus");
            names0.Add("guy");

            names0.Add("hal");
            names0.Add("han");

            names0.Add("ian");
            names0.Add("ike");

            names0.Add("jan");
            names0.Add("jax");
            names0.Add("jay");
            names0.Add("jeb");  // remember Kerbal Space program? :D
            names0.Add("jed");
            names0.Add("jen");
            names0.Add("jo");
            names0.Add("job");
            names0.Add("joh");
            names0.Add("joy");
            names0.Add("ju");

            names0.Add("kai");
            names0.Add("kai");
            names0.Add("kim");

            names0.Add("lee");
            names0.Add("lev");
            names0.Add("liv");
            names0.Add("lil");
            names0.Add("liz");
            names0.Add("lyn");

            names0.Add("mae");
            names0.Add("max");
            names0.Add("may");
            names0.Add("meg");

            names0.Add("ned");

            names0.Add("oak");


            names0.Add("pam");
            names0.Add("pax");
            names0.Add("peg");

            names0.Add("rae");
            names0.Add("raj");
            names0.Add("rex");
            names0.Add("rob");
            names0.Add("ron");
            names0.Add("roy");
            names0.Add("rue");
            names0.Add("rye");

            names0.Add("sam");
            names0.Add("seb");
            names0.Add("sky");
            names0.Add("sue");

            names0.Add("ted");
            names0.Add("tod");
            names0.Add("ty");

            names0.Add("val");
            names0.Add("van");
            names0.Add("vi");
            names0.Add("viv");

            names0.Add("wes");

            names0.Add("zev");


            names0 = names0.Distinct<string>().ToList<string>();

            List<string> names1 = new List<string>();
            names1.Add("abigail");
            names1.Add("abby");
            names1.Add("addie");
            names1.Add("adrianne");
            names1.Add("agathe");
            names1.Add("aileen");
            names1.Add("alyson");
            names1.Add("amalia");
            names1.Add("arnold");
            names1.Add("avril");

            names1.Add("beatrice");
            names1.Add("benjamin");
            names1.Add("bettina");
            names1.Add("billy");
            names1.Add("bonny");
            names1.Add("brooke");

            names1.Add("carmen");
            names1.Add("caroline");
            names1.Add("catrina");
            names1.Add("charys");
            names1.Add("cher");
            names1.Add("chloe");
            names1.Add("cliff");
            names1.Add("corey");
            names1.Add("crista");
            names1.Add("crysta");

            names1.Add("daisy");
            names1.Add("dalbert");
            names1.Add("darcy");
            names1.Add("della");
            names1.Add("diana");
            names1.Add("dolores");

            names1.Add("elina");
            names1.Add("ella");
            names1.Add("ellen");
            names1.Add("ember");
            names1.Add("eric");
            names1.Add("evelyn");

            names1.Add("fanny");
            names1.Add("faye");
            names1.Add("felicitas");
            names1.Add("francene");

            names1.Add("gabby");
            names1.Add("gabriella");
            names1.Add("giorgio");
            names1.Add("gloria");
            names1.Add("godfrey");
            names1.Add("gustavo");
            names1.Add("greta");

            names1.Add("hans");
            names1.Add("harry");
            names1.Add("heath");
            names1.Add("herbert");
            names1.Add("homer");
            names1.Add("humpfrey");

            names1.Add("isaac");
            names1.Add("isiah");
            names1.Add("ismael");
            names1.Add("ivan");

            names1.Add("jana");
            names1.Add("jayden");
            names1.Add("jeanne");
            names1.Add("joey");

            names1.Add("kendal");
            names1.Add("kevin");
            names1.Add("kevin");
            names1.Add("klaus");
            names1.Add("kurt");

            names1.Add("lauence");
            names1.Add("lawrence");
            names1.Add("letha");

            names1.Add("martin");
            names1.Add("michael");
            names1.Add("michaela");
            names1.Add("mike");
            names1.Add("mohamed");

            names1.Add("niki");
            names1.Add("norbert");

            names1.Add("oleta");
            names1.Add("orlando");
            names1.Add("oscar");
            names1.Add("oswald");
            names1.Add("otto");


            names1.Add("pablo");
            names1.Add("paco");
            names1.Add("pascal");
            names1.Add("peleg");
            names1.Add("ping");

            names1.Add("richard");
            names1.Add("roland");

            names1.Add("salvatore");
            names1.Add("scarlet");
            names1.Add("seth");
            names1.Add("shadrach");
            names1.Add("sheldon");

            names1.Add("talon");
            names1.Add("tanner");
            names1.Add("terance");
            names1.Add("terrance");
            names1.Add("thaddeus");
            names1.Add("trenton");

            names1.Add("walter");

            names1.Add("vanessa");
            names1.Add("vito");

            List<string> names2 = new List<string>();

            names2.Add("Abigail");
            names2.Add("Abby");
            names2.Add("Addie");
            names2.Add("Adrianne");
            names2.Add("Agathe");
            names2.Add("Aileen");
            names2.Add("Alyson");
            names2.Add("Amalia");
            names2.Add("Arnold");
            names2.Add("Avril");

            names2.Add("Beatrice");
            names2.Add("Benjamin");
            names2.Add("Bettina");
            names2.Add("Billy");
            names2.Add("Bonny");
            names2.Add("Brooke");

            names2.Add("Carmen");
            names2.Add("Caroline");
            names2.Add("Catrina");
            names2.Add("Charys");
            names2.Add("Cher");
            names2.Add("Chloe");
            names2.Add("Cliff");
            names2.Add("Corey");
            names2.Add("Crista");
            names2.Add("Crysta");

            names2.Add("Daisy");
            names2.Add("Dalbert");
            names2.Add("Darcy");
            names2.Add("Della");
            names2.Add("Diana");
            names2.Add("Dolores");

            names2.Add("Elina");
            names2.Add("Ella");
            names2.Add("Ellen");
            names2.Add("Ember");
            names2.Add("Eric");
            names2.Add("Evelyn");

            names2.Add("Fanny");
            names2.Add("Faye");
            names2.Add("Felicitas");
            names2.Add("Francene");

            names2.Add("Gabby");
            names2.Add("Gabriella");
            names2.Add("Giorgio");
            names2.Add("Gloria");
            names2.Add("Godfrey");
            names2.Add("Gustavo");
            names2.Add("Greta");

            names2.Add("Hans");
            names2.Add("Harry");
            names2.Add("Heath");
            names2.Add("Herbert");
            names2.Add("Homer");
            names2.Add("Humpfrey");

            names2.Add("Isaac");
            names2.Add("Isiah");
            names2.Add("Ismael");
            names2.Add("Ivan");

            names2.Add("Jana");
            names2.Add("Jayden");
            names2.Add("Jeanne");
            names2.Add("Joey");

            names2.Add("Kendal");
            names2.Add("Kevin");
            names2.Add("Kevin");
            names2.Add("Klaus");
            names2.Add("Kurt");

            names2.Add("Lauence");
            names2.Add("Lawrence");
            names2.Add("Letha");

            names2.Add("Martin");
            names2.Add("Michael");
            names2.Add("Michaela");
            names2.Add("Mike");
            names2.Add("Mohamed");

            names2.Add("Niki");
            names2.Add("Norbert");

            names2.Add("Oleta");
            names2.Add("Orlando");
            names2.Add("Oscar");
            names2.Add("Oswald");
            names2.Add("Otto");

            names2.Add("Pablo");
            names2.Add("Paco");
            names2.Add("Pascal");
            names2.Add("Peleg");
            names2.Add("Ping");

            names2.Add("Richard");
            names2.Add("Roland");

            names2.Add("Salvatore");
            names2.Add("Scarlet");
            names2.Add("Seth");
            names2.Add("Shadrach");
            names2.Add("Sheldon");

            names2.Add("Talon");
            names2.Add("Tanner");
            names2.Add("Terance");
            names2.Add("Terrance");
            names2.Add("Thaddeus");
            names2.Add("Trenton");

            names2.Add("Walter");

            names2.Add("Vanessa");
            names2.Add("Vito");


            List<string> names3 = new List<string>();
            names3.Add("Garrard");
            names3.Add("Meinhardt");

            names1.Add("Yvette");
            names1.Add("Yelena");



            names.Add(0, names0);   // easy two - four letter names
            names.Add(1, names1);   // longer names
            names.Add(2, names2);   // longer names plus CamelCase
            names.Add(3, names3);   // longer names plus CamelCase plus complicated names plus surname 
        }

        internal static void Update(float deltaT)
        {
            totalTime += deltaT;
        }





    }
}
