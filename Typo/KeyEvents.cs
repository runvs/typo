﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Typo
{
    static class KeyEvents
    {
        static KeyboardState oldState;

        static Dictionary<Keys, bool> pressedMap;
        static Dictionary<Keys, bool> releasedMap;

        static public bool Pressed(Keys k)
        {
            return pressedMap[k];
        }

        static public bool Released(Keys k)
        {
            return releasedMap[k];
        }

        static public void Initialize()
        {
            oldState = Keyboard.GetState();
            pressedMap = new Dictionary<Keys, bool>();
            releasedMap = new Dictionary<Keys, bool>();

            foreach (var kvp in KeyboardStateToChar.Map)
            {
                pressedMap.Add(kvp.Key, false);
                releasedMap.Add(kvp.Key, false);
            }
            pressedMap.Add(Keys.Enter, false);
            releasedMap.Add(Keys.Enter, false);
        }

        public static void UpdateInput()
        {
            KeyboardState newState = Keyboard.GetState();

            foreach (var kvp in KeyboardStateToChar.Map)
            {
                Keys k = kvp.Key;

                pressedMap[k] = false;
                releasedMap[k] = false;
                // Is the key down?
                if (newState.IsKeyDown(k))
                {
                    // If not down last update, key has just been pressed.
                    if (!oldState.IsKeyDown(k))
                    {
                        pressedMap[k] = true;
                    }
                }
                else if (oldState.IsKeyDown(k))
                {
                    // Key was down last update, but not down now, so
                    // it has just been released.
                    releasedMap[k] = true;
                }
            }

            Keys l = Keys.Enter;

            pressedMap[l] = false;
            releasedMap[l] = false;
            // Is the key down?
            if (newState.IsKeyDown(l))
            {
                // If not down last update, key has just been pressed.
                if (!oldState.IsKeyDown(l))
                {
                    pressedMap[l] = true;
                }
            }
            else if (oldState.IsKeyDown(l))
            {
                // Key was down last update, but not down now, so
                // it has just been released.
                releasedMap[l] = true;
            }



            oldState = newState;
        }
    }
}
