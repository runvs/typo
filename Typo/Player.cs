﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Typo
{
    class Player : GameObject
    {
        private Texture2D playerTextureRight;
        private Texture2D playerTextureUp;
        private Texture2D playerTextureLeft;
        private Texture2D playerTextureDown;
        public Vector2 position { get; set; }




        private Vector2 targetPosition;

        SpriteDirection dir;

        private float angle;


        public void Initialize()
        {
            position = GameProperties.ScreenSize * 0.5f;
            targetPosition = new Vector2(150, 150);

            NameTypedEventManager.OnSend += SetTarget;

        }

        private void SetTarget(Enemy enemy)
        {
            this.targetPosition = enemy.Position;
        }

        public void LoadContent(ContentManager content)
        {
            playerTextureRight = content.Load<Texture2D>("GFX/player_r");
            playerTextureLeft = content.Load<Texture2D>("GFX/player_l");
            playerTextureUp = content.Load<Texture2D>("GFX/player_u");
            playerTextureDown = content.Load<Texture2D>("GFX/player_d");

        }


        public void GetInput(KeyboardState KBS)
        {
            if (KBS.IsKeyDown(Keys.Right))
            {
                targetPosition.X += 5;
            }
            else if (KBS.IsKeyDown(Keys.Left))
            {
                targetPosition.X -= 5;
            }
            if (KBS.IsKeyDown(Keys.Up))
            {
                targetPosition.Y += 5;
            }
            else if (KBS.IsKeyDown(Keys.Down))
            {
                targetPosition.Y -= 5;
            }
        }

        public void Draw(SpriteBatch spb)
        {
            Texture2D currentTexture;
            if (dir == SpriteDirection.SD_DOWN)
            {
                currentTexture = playerTextureDown;
            }
            else if (dir == SpriteDirection.SD_LEFT)
            {
                currentTexture = playerTextureLeft;
            }
            else if (dir == SpriteDirection.SD_RIGHT)
            {
                currentTexture = playerTextureRight;
            }
            else
            {
                currentTexture = playerTextureUp;
            }

            Vector2 screenposition = position - Camera.CameraTotalPosition;

            Rectangle dest = new Rectangle((int)(screenposition.X) - 24, (int)(screenposition.Y) - 24, 48, 48);
            Rectangle source = new Rectangle(0, 0, 16, 16);

            spb.Draw(currentTexture, dest, source, Color.White);
        }

        public void Update(float deltaT)
        {

            DetermineDirection();
            SetDirection();

        }

        private void SetDirection()
        {

            if (angle >= 1.0 / 4.0 * Math.PI && angle < 3.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_UP;
            }
            else if (angle >= 3.0 / 4.0 * Math.PI && angle < 5.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_LEFT;
            }
            else if (angle >= 5.0 / 4.0 * Math.PI && angle < 7.0 / 4.0 * Math.PI)
            {
                dir = SpriteDirection.SD_DOWN;
            }
            else
            {
                dir = SpriteDirection.SD_RIGHT;
            }

        }

        private void DetermineDirection()
        {
            angle = GameProperties.GetAngleBewtweenPoints(position, targetPosition);
        }

    }
}
