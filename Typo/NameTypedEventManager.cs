﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Typo
{
    public static class NameTypedEventManager
    {
        public delegate void NameTyped(Enemy enemy);
        public static event NameTyped OnSend;

        public static void Call(Enemy enemy)
        {
            if (OnSend != null)
            {
                OnSend(enemy);
            }
        }

    }
}
