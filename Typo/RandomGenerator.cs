﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Typo
{
    public static class RandomGenerator
    {
        private static bool initialized = false;
        public static Random Random { get { if (!initialized) Initialize(); return random; } }
        private static Random random;
        public static void Initialize()
        {
            initialized = true;
            random = new Random();
        }

        static private void CheckRanges(ref Vector2 range)
        {

            if (range.X == range.Y)
            {
                throw new ArgumentOutOfRangeException("range", "To create Random Numbers the range must be existing.");
            }

            if (range.X > range.Y)
            {
                float tmp = range.X;
                range.X = range.Y;
                range.Y = tmp;
            }
        }

        static public Vector2 GetRandomVector2OnCircle(float radius)
        {
            double angle = Random.NextDouble() * 2 * Math.PI;
            return new Vector2((float)Math.Cos(angle) * radius, (float)Math.Sin(angle) * radius);
        }

        static public Vector2 GetRandomVector2InCircle(float radius)
        {
            double angle = Random.NextDouble() * 2 * Math.PI;
            return new Vector2((float)(Math.Cos(angle) * Random.NextDouble() * radius), (float)(Math.Sin(angle) * Random.NextDouble() * radius));
        }



        internal static float NextFloat(float p1, float p2)
        {
            float difference = p2 - p1;
            return ((float)(Random.NextDouble()) * difference) + p1;
        }
    }
}
