﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Typo
{
    public static class Camera
    {
        public static Vector2 CameraTotalPosition { get { return CameraPosition + OffsetVector + ShakeVector; } }
        public static Vector2 CameraPosition { get; set; }
        public static Vector2 CameraVelocity { get; private set; }
        public static float CameraMaxVelocity = 200.0f;

        public static Vector2 MinPosition;
        public static Vector2 MaxPosition;

        public static Vector2 ShouldBePosition { get; set; }

        public static Vector2 OffsetVector { get; private set; }
        public static Vector2 ShakeVector { get; private set; }

        public static bool IsInScreenShake { get; private set; }
        private static float ScreenShakeTime;
        private static float ScreenShakeInterval;
        private static float ScreenShakeIntervalMax;
        private static float ScreenShakeStrength;

        public static void OffsetScreen(Vector2 newOffset)
        {
            OffsetVector = newOffset;
        }

        public static void Update(float deltaT)
        {
            if (IsInScreenShake)
            {
                ScreenShakeTime -= deltaT;
                if (ScreenShakeTime <= 0)
                {
                    IsInScreenShake = false;
                    ShakeVector = new Vector2(0, 0);
                }
                else
                {
                    ScreenShakeInterval -= deltaT;
                    if (ScreenShakeInterval <= 0)
                    {
                        ScreenShakeInterval = ScreenShakeIntervalMax;
                        ShakeVector = RandomGenerator.GetRandomVector2InCircle(ScreenShakeStrength);
                    }
                }
            }

            float OffsetVectorLength = OffsetVector.Length();
            if (OffsetVectorLength >= 1)
            {
                OffsetVector *= 0.96f;
            }

        }

        public static void ScreenShake(float strength, float duration, float interval)
        {
            IsInScreenShake = true;
            ScreenShakeTime = duration;
            ScreenShakeStrength = strength;
            ScreenShakeInterval = 0.0f;
            ScreenShakeIntervalMax = interval;
        }

    }
}
